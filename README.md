# penv

**penv** is a minimalist Python version manager, currently 189 lines of Bash.

This is a modified version of [p](https://github.com/qw3rtman/p) with the goal of being as simple and understandable as possible.

## Install requirements

* `cURL`
* `$PATH` variable set such that the `$PYTHON_EXECUTABLE` variable is found before the standard `/usr/bin/python`, e.g. prefix your `$PATH` with `$HOME/.local/bin` (a common place to store user scripts)

## Installation

Simply copy the `penv` script to a location specified in the `$PATH` and make it executable.

For example:

```
export PATH="$PATH:$HOME/.local/bin" <-- add this line to your .bashrc or similar file
```

and

```
cp penv ~/.local/bin/penv
chmod +x ~/.local/bin/penv
```

## Why?

Python version management seems to be unintuitive and complex. Anaconda is 600MB, `penv` is 8kb. I just want to have a version manager that I can understand and won't fail.

### How to use penv with virtualenv etc.

The two processess are kept completely separate:
```
penv <version> <-- sets the Python version, shell might need to be refreshed
python -m venv <venv-dir> <-- sets virtual environment using Python set by penv
```

### Why doesn't the script have feature X?

Simplicity.

## Usage

```
Usage: penv [COMMAND]

  Commands:
    penv                              Display this message and exit
    penv ls                           Show penv-installed Python versions
    penv <version>                    Activate to Python <version>
      penv default                    Activate to the default system Python
```

## Removing environments

Just go to `$PENV_VERSIONS_DIR` (default `$HOME/.local/penv/versions`) and remove any version you like.

You can remove everything done by `penv` by removing the dir itself (`$HOME/.local/penv`) and by removing the `$PYTHON_EXECUTABLE` symlink (default in `$HOME/.local/bin/python`)

## How does it work?

`penv` works by downloading and compiling Python to a specified directory and symlinking a local Python executable to the Python version specified. To revert back into using system Python, the script just deletes the symlink

# Licence

The original `p` is MIT licenced and at the moment this script is heavily based on `p`. Thus, `penv` also comes with MIT license.
