#!/usr/bin/env bash

# penv is a simple Python version manager written in bash
# Originally forked from https://github.com/qw3rtman/p but made more minimal
# For the script to work, your $PATH variable must have $PYTHON_EXECUTABLE (default ~/.local/bin) before /usr/local/bin

# The script requires cURL

PENV_VERSIONS_DIR=$HOME/.local/penv/versions # This is the directory to store different Python versions
PYTHON_EXECUTABLE=$HOME/.local/bin/python # This is the executable of the Python interpreter

#
# Python source mirror used in downloading different versions
#

MIRROR=(${PYTHON_MIRROR-https://www.python.org/ftp/python/})

test -d $PENV_VERSIONS_DIR/python || mkdir -p $PENV_VERSIONS_DIR/python

#
# Output usage information.
#

display_help() {
  cat <<-EOF

Usage: penv [COMMAND] [args]

  Commands:
    penv                              Display this message and exit
    penv ls                           Show installed Python versions
    penv <version>                    Activate to Python <version>
      penv default                    Activate to the default system Python
EOF
    exit 0
}

#
# Gets current human-readable Python version.
#
get_current_version() {
    # current=$(python -c 'import sys; print(".".join(map(str, sys.version_info[:3])))')
    local version=$(python -V 2>&1)
    current=${version#*Python }
}

#
# Check for installed version, and populate $active
#

check_current_version() {
    command -v python &> /dev/null
    if test $? -eq 0; then
        get_current_version
        if diff &> /dev/null \
            $PENV_VERSIONS_DIR/python/$current/python \
            $(which python) ; then
            active="python/$current"
        fi
    fi
}

#
# Display sorted versions directories paths.
#

versions_paths() {
    find $PENV_VERSIONS_DIR -maxdepth 2 -type d \
        | sed 's|'$PENV_VERSIONS_DIR'/||g' \
        | egrep "[0-9]+\.[0-9]+\.[0-9]+([a|b]?)([0-9]?)+" \
        | sort -k 1,1n -k 2,2n -k 3,3n -t . -k 4,4n -d -k 5,5n -r
}

#
# List installed versions.
#

list_versions_installed() {
    for version in $(versions_paths); do
        echo ${version}
    done
}

#
# Check if the HEAD response of <url> is 200.
#

is_ok() {
    curl -Is $1 | head -n 1 | grep 200 > /dev/null
}

#
# Determine tarball url for <version>
#

tarball_url() {
    version_directory="${version%a*}"
    echo "${MIRROR}${version_directory%b*}/Python-${version}.tgz"
}

#
# Activate <version>
#

activate() {
    local version=$1
    check_current_version
    if test "$version" != "$active"; then
        ln -sf $PENV_VERSIONS_DIR/$version/python $PYTHON_EXECUTABLE
	printf "Create symbolic link from: \n $PENV_VERSIONS_DIR/$version/Include \nto\n /usr/include/penv?\n"
	read -n1 -p "This operation requires sudo priveleges. [y/n] " response
	printf "\n"
	case "$response" in
   	    [yY]) sudo ln -v -s $PENV_VERSIONS_DIR/$version/Include /usr/include/penv;;
   	    *) echo "Symbolic link not created.";;
	esac
    fi
}

#
# Activate default (system) Python by removing the symlink
#
activate_default() {
    rm -iv $PYTHON_EXECUTABLE
    [ -f /usr/include/penv ] && printf "Removing symbolic link from /usr/include/penv\n" && sudo rm -iv /usr/include/penv
}

#
# Install <version>
#

install() {
    local version=${1#v}

    local dots=$(echo $version | sed 's/[^.]*//g')
    if test ${#dots} -eq 1; then
        version=$($GET 2> /dev/null ${MIRROR} \
            | egrep -o '[0-9]+\.[0-9]+\.[0-9]+' \
            | egrep -v '^0\.[0-7]\.' \
            | egrep -v '^0\.8\.[0-5]$' \
            | sort -u -k 1,1n -k 2,2n -k 3,3n -t . \
            | egrep ^$version \
            | tail -n1)

        test $version || echo "invalid version ${1#v}"
    fi

    local dir=$PENV_VERSIONS_DIR/python/$version
    local url=$(tarball_url $version)

    if test -d $dir; then
        if [[ ! -e $dir/n.lock ]] ; then
            activate python/$version
            get_current_version
            echo "Now using Python $current!"

            exit
        fi
    fi

    is_ok $url || abort "invalid version $version"

    mkdir -p $dir
    touch $dir/penv.lock
    cd $dir
    curl -L# $url | tar -zx --strip 1
    rm -f $dir/penv.lock
    ./configure &> /dev/null
    make &> /dev/null

    if [ ! -f "python" ]; then
    	abort "Unable to compile Python $version!"
    fi

    activate python/$version
    hash -r
    get_current_version
    echo "Now using Python $current!"

}

#
# Handle arguments.
#

if test $# -eq 0; then
    display_help
else
    while test $# -ne 0; do
        case $1 in
            ls) list_versions_installed; exit ;;
            default) activate_default; exit ;;
            *) install $1; exit ;;
        esac
        shift
    done
fi
